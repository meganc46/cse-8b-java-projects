//------------------------------------------------------------------//
// Board.java                                                       //
//                                                                  //
// Class used to represent a 2048 game board                        //
//                                                                  //
// Author:  Brandon Williams                                        //
// Date:    1/17/15                                                 //
//------------------------------------------------------------------//


/*
 * Name: Megan Bella Chang
 * Login:cs8bakj
 * Date: Feb 5  2015
 * File: Board.java
 * Sources of Help: Oracle JavaDoc, Java API
 * Program Description: Board.java manages the board in the game. It creates
 *   saves and loads the board, which is used in the GameManager.java class.
 *   It also contains methods for checking if a move is valid, and for 
 *   for performing those moves. It checks if the game is over too.
 */


/**     Sample Board
 *
 *      0   1   2   3
 *  0   -   -   -   -
 *  1   -   -   -   -
 *  2   -   -   -   -
 *  3   -   -   -   -
 *
 *  The sample board shows the index values for the columns and rows
 */

import java.util.*;
import java.io.*;


/*
 * Name: Board (Class)
 * Purpose: Board class contains constructors/methods to create, save,
 *   and load boards to be used in GameManager.java. It also moves, 
 *   merges, and adds new tiles.
 * Parameters: n/a
 * Return: n/a
 */

public class Board
{
  //Instance variables
  public final int NUM_START_TILES = 2;
  public final int TWO_PROBABILITY = 90;
  public final int GRID_SIZE;

  // Instance variable used in moveDIRECTION helper methods 
  public final int MERGE_VALUE = 2;

  private final Random random;
  private int[][] grid;
  private int score;

  // Instance variable used in Board Constructor
  private final int boardSizeVar = 4;

  // Constructs a fresh board with random tiles
  public Board(int boardSize, Random random)
  {
    this.random = random;
    if (boardSize < NUM_START_TILES)
      boardSize = boardSizeVar;
    GRID_SIZE = boardSize;
    grid = new int[GRID_SIZE][GRID_SIZE];
    for (int i = 0; i < NUM_START_TILES; i++){ 
      this.addRandomTile();
    }
  }

  // Construct a board based off of an input file 
  public Board(String inputBoard, Random random) throws IOException
  {
    this.random = random;

    // uses scanner to load input board file
    Scanner input = new Scanner(new File(inputBoard));
    int inputt = (input.nextInt());
    GRID_SIZE = inputt;
    score = (input.nextInt());

    // creates new grid and populates it
    grid = new int[GRID_SIZE][GRID_SIZE];
    for (int i = 0; i < GRID_SIZE; i++){
      for (int j = 0; j < GRID_SIZE; j++){
        grid[i][j] = input.nextInt();
      }
    }
  }

  // Saves the current board to a file, uses PrintWriter to print
  // board to terminal
  public void saveBoard(String outputBoard) throws IOException
  {
    PrintWriter out = new PrintWriter(outputBoard); 
    out.println(this.GRID_SIZE);
    out.println(this.score);
    for ( int i = 0; i < GRID_SIZE; i++){
      for ( int j = 0; j < GRID_SIZE; j++){
        out.print(grid[i][j] + " ");
      }
      out.print("\n");
    }
    out.close();    
  }

  /*
   * Name: addRandomTile (method)
   * Purpose: method adds a random tile (of value 2 or 4) to a random 
   *   empty space on the board
   * Parameters: none
   * Return: void - returns nothing
   */

  public int getScore() {
    return score;
  }
  
  /*
  public int getSize(){
    int boardSize = (GRID_SIZE)*(GRID_SIZE);
    System.out.println(boardSize);
    return boardSize;
  }
  */

  public void addRandomTile()
  {
    // for all tiles on board, count # of available tiles (count)
    int count = 0;
    for (int i = 0; i < GRID_SIZE; i++){
      for (int j = 0; j < GRID_SIZE; j++){

        // for all empty/available tiles, increment count
        if (grid[i][j] == 0)
          count++;
      }
    }

    // if count is not equal to 0
    if (count != 0){

      // creates random int called location between 0 and count-1
      int location = random.nextInt(count)+1;

      // creates random int called value between 0 and 99
      int value = random.nextInt(100);

      // creates variable locchecker to check location and store location of
      // available tiles
      int locchecker = 0;

      // walk the board row/column
      for (int k = 0; k < GRID_SIZE; k++){
        for (int m = 0; m < GRID_SIZE; m++){

          // if grid tile contains value of 0, increment locchecker to 
          // represent number of available tiles
          if (grid [k][m] == 0){
            locchecker++;
          }

          // if random number location is equal to locchecker, and grid 
          // tiles contain value of 0
          if (locchecker == location && grid[k][m]==0){ 

            // if random value generated is less than 90 (90% chance), set
            // tile value to 2, otherwise set to 4
            if (value < TWO_PROBABILITY)
              grid[k][m] = 2;
            else
              grid[k][m] = 4;
          }
        }   
      }
    }
  }

  /* Name: isGameOver(method)
   * Purpose: This method checks to see if game is over. The game ends when
   *  there are no more valid moves left and when there are no empty tiles.
   * Parameters: none
   * Return: boolean - when game is over, return true, otherwise return false.
   */

  public boolean isGameOver()
  {
    // loops through grid
    for (int i = 0; i < GRID_SIZE; i++){
      for (int j = 0; j < GRID_SIZE; j++){

        //if there are no more valid moves, or no empty tiles, game is over
        if (!this.canMove(Direction.RIGHT) && !this.canMove(Direction.DOWN) && 
            !this.canMove(Direction.LEFT) && !this.canMove(Direction.UP) && 
            grid[i][j] !=0)
          return true;
      }
    }
    return false;
  }

  /* Name: canMove (method)
   * Purpose: This method checks if board can move in direction specified. It 
   *  can move if the tile immediately below/above/right/left of it is of equal
   *  value, or if it is empty tile. If not, it can't move. See helper methods
   *  below for more details.
   * Parameters: Direction - represents the direction board wants to move in
   * Return: boolean - if can move, return true, and if not, return false
   */

  public boolean canMove(Direction direction)
  {
    // if inputted direction is a valid input, and move is valid, return true
    if (direction == direction.LEFT && canMoveLeft())
      return true;
    if (direction == direction.RIGHT && canMoveRight())
      return true;
    if (direction == direction.UP && canMoveUp())
      return true;
    if (direction == direction.DOWN && canMoveDown())
      return true;

    // else, return false
    else 
      return false;
  }

  /* Name: canMoveLeft (helper method)
   * Purpose: This method checks if board can move left.
   * Parameter: none
   * Return: boolean - if can move, return true, otherwise return false
   */

  public boolean canMoveLeft()
  {
    // loops through grid, row first then column (skips first column because
    // first colunn can never move left)
    for (int i = 0; i < GRID_SIZE; i++){
      for (int j = 1; j < GRID_SIZE; j++){

        // if tile not empty (ie. contains value), and if either the grid to
        // its left is empty or of equal value, tile can move, and return true
        if (grid[i][j] != 0 && (grid[i][j-1]==0 || grid[i][j-1] == grid[i][j]))
          return true;
      }
    }
    return false;
  }

  /* Name: canMoveRight() (helper method)
   * Purpose: This method checks if board can move right.
   * Parameter: none
   * Return: boolean - if can move, return true, otherwise return false
   */

  public boolean canMoveRight()
  {
    // loops through grid, row first then column (skips last column because
    // last column can never move right)
    for (int i = 0; i < GRID_SIZE; i++){
      for (int j = 0; j < GRID_SIZE-1; j++){

        // if tile not empty (ie. contains value), and if either grid to right
        // is empty or of equal value, tile can move, and return true
        if (grid[i][j] != 0 && (grid[i][j+1]==0 || grid[i][j+1] == grid[i][j]))
          return true;
      }
    } 
    return false;
  }

  /* Name: canMoveUp (helper method)
   * Purpose: This method checks if board can move up.
   * Parameter: none
   * Return: boolean - if can move, return true, otherwise return false
   */ 

  public boolean canMoveUp()
  {
    // loops through grid, row first, then column (skips first row because
    // top row can never move up)
    for (int i = 1; i < GRID_SIZE; i++){
      for (int j = 0; j < GRID_SIZE; j++){

        // if tile is not empty (ie. contains value), and if either grid above
        // is empty or of equal value, tile can move, and return true
        if (grid[i][j] != 0 && (grid[i-1][j]==0 || grid[i-1][j] == grid[i][j]))
          return true;
      }
    } 
    return false;
  }

  /* Name: canMoveDown (helper method)
   * Purpose: This method checks if board can move down
   * Parameter: none
   * Return: boolean - if can move, return true, otherwise return false
   */

  public boolean canMoveDown()
  {
    // loops through grid, row first, then column (skips last row because
    // last row can never move down)
    for (int i = 0; i < GRID_SIZE-1; i++){
      for (int j = 0; j < GRID_SIZE; j++){

        // if tile is not empty (ie. contains value), and if either grid below
        // is empty or of equal value, tile can move, and return true
        if (grid[i][j] != 0 && (grid[i+1][j]==0 || grid[i+1][j] == grid[i][j]))
          return true;
      }
    }
    return false;
  } 

  /* Name: move (method)
   * Purpose: This method actually moves the board. It is called in GameManager
   *  after canMove returns true. See helper methods below for details.
   * Parameter: Direction - represents direction board will move in 
   * Return: boolean - will return true when if will move (false only serves
   *  as "placeholder", should never return false)
   */

  public boolean move(Direction direction)
  {
    // if inputted direction equals letter assigned to actual direction (wasd), move 
    if (direction.equals(Direction.LEFT)){
      moveLEFT();
      return true;
    }
    else if(direction.equals(Direction.RIGHT)){
      moveRIGHT();
      return true;
    }
    else if (direction.equals(Direction.UP)){
      moveUP();
      return true;
    }
    else if (direction.equals(Direction.DOWN)){
      moveDOWN();
      return true;
    }
    else
      return false;

  }

  /* Name: moveUp (helper method)
   * Purpose: This method moves the board up. The grid is accessed by an 
   *  an arraylist. Within the arraylist, tiles are merged. Score is also
   *  incremented. After tiles are modified within arraylist, transfer
   *  arraylist back to grid, and clear arraylist for next column.
   * Parameters: none
   * Return: boolean - should always return true if method is called
   */ 

  public boolean moveUP()  
  {
    // new arraylist constructed (empty for now)
    ArrayList<Integer> list = new ArrayList<Integer>();

    // loop thru each column in grid
    for (int j = 0; j < GRID_SIZE; j++){

      // create variable to hold number of empty tiles in column not
      // added to arraylist
      int zerocounter = 0;

      // loop through each row in grid
      for (int i = 0; i < GRID_SIZE; i++)
      {
        // if tile is not empty, add it to arraylist
        if (grid[i][j]!= 0){
          list.add(grid[i][j]);
        }

        // if it is empty, increment zerocounter
        else
          zerocounter++;
      }

      // loop through arraylist
      for (int k = 0; k < list.size()-1; k++)
      {
        // if tile at index is equal to the tile to its right
        if (list.get(k).equals(list.get(k+1)))
        {
          // set tile to right of index to double the value of tile at index, 
          // effectively "merging" the two values
          list.set(k+1, list.get(k) * MERGE_VALUE);

          // remove the tile at index
          list.remove(k);

          // add new tile with value 0 to compensate for previously removed index
          list.add(new Integer(0));

          // increment score by value of "merged" tile
          score += (list.get(k).intValue());
        }
      }

      // add number of empty tiles to end of arraylist (number retrieved
      // from zerocounter)
      for (int z = 0; z < zerocounter; z++){
        list.add(new Integer(0));
      }

      // loop through full-sized arraylist, add it back to its respective 
      // position in grid
      for (int m = 0; m < list.size(); m++){
        grid[m][j]= list.get(m).intValue();
      }

      // clear arraylist for next column
      list.clear();
    }
    return true;
  }  

  /* Name: moveLEFT (helper method)
   * Purpose: THis method moves the board left. The grid is accessed by an
   *  arraylist. Within the arraylist, tiles are merged. Score is also
   *  incremneted. After the tiles are modified within the arraylist,
   *  transfer the arraylist back to grid, and clear it for next row
   * Parameters: none
   * Return: boolean = should alway return true if method is called
   */

  public boolean moveLEFT()
  {
    // new arraylist constructed (empty for now)
    ArrayList<Integer> list = new ArrayList<Integer>();

    // loop through each row in grid
    for (int i = 0; i < GRID_SIZE; i++){

      // create variable to hold number of empty tiles in row not 
      // added to arraylist
      int zerocounter = 0;

      // loop through each column in grid
      for (int j = 0; j < GRID_SIZE; j++){

        // if tile is not empty, add it to arraylist
        if (grid[i][j] != 0){
          list.add(grid[i][j]);
        }

        // if it is empty, increment zerocounter
        else
          zerocounter++;
      }

      // loop through arraylist
      for (int k = 0; k < list.size()-1; k++){

        // if tile at index is equal to th tile to its right
        if (list.get(k).equals((list.get(k+1)))){

          // set tile at index to double the value of tile to right of index
          // effectively "merging" the two values
          list.set(k, MERGE_VALUE*list.get(k+1));

          // remove the tile 
          list.remove(k+1);

          // add new tile with value 0 to compensate for previously removed
          list.add(new Integer(0));

          // increment score by value of "merged" tile
          score += (list.get(k).intValue());
        }
      }

      // add number of empty tiles to end of arraylist (number retrieved
      // from zerocounter)
      for (int z = 0; z < zerocounter; z++){
        list.add(new Integer(0));
      }

      // loop through full-sized arraylist, add it back to its respective 
      // position in grid
      for (int m = 0; m < list.size(); m++){
        grid[i][m] = list.get(m).intValue();
      }

      // clear arraylist for next row
      list.clear();
    }
    return true;
  }

  /* Name: moveRight (helper method)
   * Purpose: This method moves the board right. The grid is accessed by 
   *  an arraylist. Within the arraylist, tiles are merged. Score is also
   *  incremented. After tiles are modified within arraylist, transfer
   *  arraylist back to grid, and clear arraylist for next row.
   * Parameter: none
   * Return: boolean - should always return true if method is called
   */ 

  public boolean moveRIGHT()
  {
    // create new araylist constructed (empty for now)
    ArrayList<Integer> list = new ArrayList<Integer>();

    // loop through each row in grid
    for (int i = 0; i < GRID_SIZE; i++){

      // create variable to hold number of eampty tiles in row not 
      // added to arraylist
      int zerocounter = 0;

      // loop through each column in grid
      for (int j = 0; j < GRID_SIZE; j++){

        // if tile is not empty, add it to arraylist
        if (grid[i][j] != 0){
          list.add(grid[i][j]);
        }

        // if it is empty, increment zerocounter
        else
          zerocounter++;
      }

      // loop through arraylist
      for (int k = list.size()-1; k > 0; k--){

        // it tile at index is equal to tile to its left
        if(list.get(k).equals((list.get(k-1)))){

          // set tile at index to double the value of tile to left of index,
          // effectivley "merging" the two values
          list.set(k, list.get(k-1)*MERGE_VALUE);

          // remove the tile to left of index
          list.remove(k-1);

          // add new tile with value 0 to compensate for previously remved tile
          list.add(0, new Integer(0));

          // increment score by value of "merged" tile
          score += (list.get(k).intValue());
        }
      }

      // add number of empty tiles to end of arraylist (number retrieved 
      // from zerocounter)
      for (int z = 0; z < zerocounter; z++){
        list.add(0, new Integer(0));
      }

      // loop through full-sized array, add it back to its respective 
      // posiiton in grid
      for (int m = 0; m < list.size(); m++){
        grid[i][m] = list.get(m).intValue();
      }

      // clear arraylist for next row
      list.clear();
    }
    return true;
  }

  /* Name: moveDOWN (helper method)
   * Purpose: This method moves the board down. the grid is accessed by
   *  an arraylist. wihtin the arraylist, tiles are merged. score is also
   *  incremented. after tiles are modified within arraylist, transfer 
   *  arraylist back to grid, and clear arraylist for next column
   * Parameter: none
   * Return: boolean - should always return true if method is called
   */

  public boolean moveDOWN()
  {
    // create new arraylist constructed (empty for now)
    ArrayList<Integer> list = new ArrayList<Integer>();

    // loop through each column in grid
    for(int j = 0; j < GRID_SIZE; j++){

      // create variable to hold number of empty tiles in column 
      // not added to arraylist
      int zerocounter = 0;

      // loop through each row in grid
      for (int i = 0; i < GRID_SIZE; i++){

        // if tile is not empty, add it to arraylist
        if (grid[i][j] != 0){
          list.add(grid[i][j]);

        }

        // if it is empty, increment zerocounter
        else 
          zerocounter++;
      }

      // loop through arraylist
      for (int k = list.size()-1; k > 0; k--){

        // if tile at index is equal to tile to its left
        if (list.get(k).equals(list.get(k-1))){

          // set tile to left of index to double the value of tile at index
          // effectively "merging" the two values
          list.set(k-1,MERGE_VALUE*list.get(k));

          // remove the tile at index
          list.remove(k);

          // add new tile with value 0 to compensate for previouly removed tile
          list.add(0,new Integer(0));

          //increment score by value of "merged" tile
          score += (list.get(k).intValue());
        }
      }

      // add number of empty tiles to end of arraylist (number retrieved
      // from zero counter)
      for (int z = 0; z < zerocounter; z++){
        list.add(0, new Integer(0));
      }

      // loop through full-sized array, add it back to its repsective 
      // position in grid
      for (int m = 0; m < list.size(); m++){
        grid[m][j] = list.get(m).intValue();
      }

      // clear arraylist for next column
      list.clear();
    }
    return true;
  }

  // Return the reference to the 2048 Grid, used in GameManager
  public int[][] getGrid()
  {
    return grid;
  }
  
  // converts board to string, called in GameManager
  @Override
    public String toString()
    {
      StringBuilder outputString = new StringBuilder();
      outputString.append(String.format("Score: %d\n", score));
      for (int row = 0; row < GRID_SIZE; row++)
      {
        for (int column = 0; column < GRID_SIZE; column++)
          outputString.append(grid[row][column] == 0 ? "    -" :
              String.format("%5d", grid[row][column]));

        outputString.append("\n");
      }
      return outputString.toString();
    }
}
