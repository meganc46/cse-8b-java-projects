/*
 * Name: Megan B Chang
 * Login: cs8bakj
 * Date: March 5 2015
 * Name of File: Gui2048.java
 * Sources of Help: textbook, oracle javadocs
 * Program Description: This program uses JavaFX to create graphical interface
 *    to the 2048 game, and makes the game playable by arrow keys using event
 *    handler. The result is a fully functional 2048 game, imitating the 
 *    original game.
 */



/** Gui2048.java */
/** PA8 Release */

import javafx.application.*;
import javafx.scene.control.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.text.*;
import javafx.geometry.*;
import java.util.*;
import java.io.*;


/*
 * Name: Gui2048 (class)
 * Description: This class extends the Application class. It contains code for
 *    GUI and a keyhandler class.
 */

public class Gui2048 extends Application
{
  private String outputBoard; // The filename for where to save the Board
  private Board board; // The 2048 Game Board

  // Fill colors for each of the Tile values
  private static final Color COLOR_EMPTY = Color.rgb(238, 228, 218, 0.35);
  private static final Color COLOR_2 = Color.rgb(238, 228, 218);
  private static final Color COLOR_4 = Color.rgb(237, 224, 200);
  private static final Color COLOR_8 = Color.rgb(242, 177, 121);
  private static final Color COLOR_16 = Color.rgb(245, 149, 99);
  private static final Color COLOR_32 = Color.rgb(246, 124, 95);
  private static final Color COLOR_64 = Color.rgb(246, 94, 59);
  private static final Color COLOR_128 = Color.rgb(237, 207, 114);
  private static final Color COLOR_256 = Color.rgb(237, 204, 97);
  private static final Color COLOR_512 = Color.rgb(237, 200, 80);
  private static final Color COLOR_1024 = Color.rgb(237, 197, 63);
  private static final Color COLOR_2048 = Color.rgb(237, 194, 46);
  private static final Color COLOR_OTHER = Color.BLACK;
  private static final Color COLOR_GAME_OVER = Color.rgb(238, 228, 218, 0.73);
  //private static final Color COLOR_BACKGROUND = Color.rgb(187,173,160);
  
  //for tiles >= 8
  private static final Color COLOR_VALUE_LIGHT = Color.rgb(249, 246, 242);
  //for tiles < 8
  private static final Color COLOR_VALUE_DARK = Color.rgb(119, 110, 101);

  //Creates new gridpane
  GridPane gpane = new GridPane();
  
  //Creates new rectangle array of size 16
  Rectangle[] rect = new Rectangle[16];

  //Creates new text array of size 16
  Text[] text = new Text[16];

  //Instantiates scoreValue variable used in Start and updateBoard
  Text scoreValue;

  //Creates new index variable to keep track of rectangle/text array
  int index = 0;

  //Creates new index variable to keep track of game over
  int GOindex = 0;
  
  //Insets used to set padding
  Insets insets = new Insets(15,15,15,15);
  
  //Variables to set Hgap, Vgap
  double Hgap = 5.5;
  double Vgap = 5.5;
  
  String title_a = "2048";
  
  //Variables used in multiple methods and for multiple purposes
  int two = 2;
  int three = 3;
  int four = 4;
  int eight = 8;
  int sixteen = 16;
  int thirtyTwo = 32;
  int fifty = 50;
  int fiftyFive = 55;
  int sixty = 60;
  int sixtyFour = 64;
  int oneHundred = 100;
  int oneTwentyEight = 128;
  int twoFiftySix = 256;
  int fiveHundredTwelve = 512;
  int tenTwentyFour = 1024;
  int twentyFortyEight = 2048;
  int oneThousand = 1000;

  int sceneWidth = 500;
  int sceneHeight = 600;

  int font_a = 16;
  int font_b = 20;
  int font_c = 22;  

  /*
   * Name: start (method)
   * Purpose: This method is responsible for loading the window and board on 
   *    startup. It is also responsible for formatting.
   * Parameters: Stage primaryStage
   * Return: void
   */

  @Override
    public void start(Stage primaryStage){

      // Process Arguments and Initialize the Game Board
      processArgs(getParameters().getRaw().toArray(new String[0]));
      
      //Sets alignment of gridpane in window
      gpane.setAlignment(Pos.CENTER);
      GridPane.setHalignment(gpane, HPos.CENTER);
      GridPane.setValignment(gpane, VPos.CENTER);
      
      //Sets color of background in window
      gpane.setStyle("-fx-background-color: rgb(187,173,160)");
      
      //Sets space between rows/columns and on edges of window
      gpane.setPadding(insets);
      gpane.setHgap(Hgap);
      gpane.setVgap(Vgap);
      
      //Creates title of game, formats the text, adds it to the gridpane, 
      //aligns it
      Text title = new Text(title_a);
      title.setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
        FontPosture.ITALIC, font_b));
      gpane.add(title,0,0,two,1);
      gpane.setHalignment(title,HPos.CENTER);
      
      //Creates Score label of game, formats text, adds it to gridpane, 
      //aligns it
      Text score = new Text("Score: ");
      score.setFont(Font.font("Helvetica Neue", FontWeight.BOLD, font_a));
      gpane.add(score,two,0,two,1);
      
      //Retrieves score value of game from Board.java, formats text, adds it to
      //gridpane, aligns it
      scoreValue = new Text("" + board.getScore());
      scoreValue.setFont(Font.font("Helvetica Neue", FontWeight.BOLD, font_a));
      gpane.add(scoreValue,three,0,1,1);
      GridPane.setHalignment(scoreValue, HPos.LEFT);
      
      //Calls updateBoard method to print board at startup
      updateBoard();

      //Creates new scene, adds gridpane to it, registers keyhandler to it
      Scene scene = new Scene (gpane,sceneWidth,sceneHeight);
      scene.setOnKeyPressed(new KeyHandler());

      //Sets title for primaryStage, sets scene, and shows the primaryStage
      primaryStage.setTitle("Gui2048");
      primaryStage.setScene(scene);
      primaryStage.show();
    }

  /*
   * Name: updateBoard (method)
   * Purpose: Prints out the state of the board after event (either at startup
   *    or after every keystroke. Retrieves the state of the board from 
   *    Board.java. Called in start method and keyhandler.
   * Paramters: none
   * Return: none
   */
  
  private void updateBoard(){
    
    //Removes score from previous move
    removeScore();

    //Retrieves scorevalue of current move from Board.java, formats text, adds
    //scorevalue to gridpane, aligns it
    scoreValue = new Text("" + board.getScore());
    scoreValue.setFont(Font.font("Helvetica Neue", FontWeight.BOLD, font_a));
    gpane.add(scoreValue,three,0,1,1);
    GridPane.setHalignment(scoreValue, HPos.LEFT);
    
    //Clears board for next move
    removeBoard();
    
    //Creates new int array, fills it with Grid retrieved from Board.java
    int[][] grid = board.getGrid();

    //Loops through board...
    for (int i = 0; i < board.GRID_SIZE; i++){
      for (int j = 0; j < board.GRID_SIZE; j++){
        
        //Creates new rectangle array
        rect[index] = new Rectangle (oneHundred,fifty,fiftyFive,fiftyFive);
        
        //Fills text at each index with value of tile retrieved from Board.java
        text[index] = new Text("" + grid[i][j]);
        
        //For each tile value, set assigned color, format text
        if (grid[i][j] == 0){
          rect[index].setFill(COLOR_EMPTY);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }  
        else if (grid[i][j] == two){
          rect[index].setFill(COLOR_2);
          text[index].setFill(COLOR_VALUE_DARK);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == four){
          rect[index].setFill(COLOR_4);
          text[index].setFill(COLOR_VALUE_DARK);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == eight){
          rect[index].setFill(COLOR_8);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == sixteen) {
          rect[index].setFill(COLOR_16);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == thirtyTwo){
          rect[index].setFill(COLOR_32);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == sixtyFour){
          rect[index].setFill(COLOR_64);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == oneTwentyEight) {
          rect[index].setFill(COLOR_128);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
            font_b));
        }
        else if (grid[i][j] == twoFiftySix){
          rect[index].setFill(COLOR_256);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD,
            font_b));
        }
        else if (grid[i][j] == fiveHundredTwelve){
          rect[index].setFill(COLOR_512);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD,
            font_b));
        }
        else if (grid[i][j] == tenTwentyFour){
          rect[index].setFill(COLOR_1024);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD,
            font_a));
        }
        else if (grid[i][j] == twentyFortyEight){
          rect[index].setFill(COLOR_2048);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD,
            font_a));
        }
        else{
          rect[index].setFill(COLOR_OTHER);
          text[index].setFill(COLOR_VALUE_LIGHT);
          text[index].setFont(Font.font("Helvetica Neue", FontWeight.BOLD,
            font_a));
        }

        //Align text and rectangle in gridpane
        GridPane.setHalignment(text[index], HPos.CENTER);
        GridPane.setValignment(text[index], VPos.CENTER);
        GridPane.setHalignment(rect[index], HPos.CENTER);
        GridPane.setValignment(rect[index], VPos.CENTER);
        
        //Creates new empty text
        Text empty = new Text("");

        //Add rectangle to gridpane
        gpane.add(rect[index], j, i+1);
        
        //If value of tile is 0, don't display any text on new board
        if (grid[i][j] == 0){
          gpane.add(empty, j, i+1);
        }

        //Else if value of tile isn't 0, add new text to gridpane
        else {
          gpane.add(text[index], j, i+1);
        }

        //Increment index (should stop at 15 after finishes running through
        //each rectangle/text in array)
        index++;
      }
    }

    //Resets index to 0 
    index=0;
  }
  

  /*
   * Name: removeBoard (method)
   * Purpose: This method clears the gridpane from the previous move so that 
   *    the board can be updated in the next move
   * Paramters: none
   * Returnn: void
   */

  public void removeBoard() {
    
    //Loops through board
    for (int i = 0; i < board.GRID_SIZE; i++){
      for (int j = 0; j < board.GRID_SIZE; j++){
        
        //Removes rectangle and text at each index of array
        gpane.getChildren().remove(rect[index]);
        gpane.getChildren().remove(text[index]);

        //Increments index
        index++;
      }
    }
    
    //Resets index to 0
    index = 0;
  }
  
  /*
   * Name: removeScore (method)
   * Purpose: This method removes the score value from the current board so 
   *    that a new score value can be updated after the next move
   * Paramters: none
   * Return: none 
   */
  
  public void removeScore(){
    
    //Sets scorevalue to empty text, effectively clearing it
    scoreValue.setText(" ");
  }
  
  /*
   * Name: KeyHandler (inner class)
   * Purpose: The KeyHandler class responds to every event occured, or every
   *    key pressed. The board is updated after every key pressed. It 
   *    implements the EventHandler interface.
   */

  private class KeyHandler implements EventHandler<KeyEvent> {

    /*
     * Name: handle (method)
     * Purpose: This method corresponds each key pressed with a move, and 
     *    calls helper methods to update the new state of the board.
     *    It overrides the method in the interface.
     * Parameters: KeyEvent e (the key pressed)
     * Return: none
     */

    @Override
      public void handle(KeyEvent e){
          
        //If the key pressed is the up arrow key, check if up is a valid move,
        //move, add random tile, print indicator in terminal, clear current
        //board, update gridpane with new board
        if (e.getCode().equals(KeyCode.UP)){
          if (board.canMove(Direction.UP)){
            board.move(Direction.UP);
            board.addRandomTile();
            System.out.println("Moving Up");
            removeBoard();
            updateBoard();
          }
        }
        
        //If the key pressed is the down arrow key, check if down is a valid 
        //move, move, add random tile, print indicator in terminal, clear 
        //current board, update gridpane with new board
        if (e.getCode().equals(KeyCode.DOWN)){
          if (board.canMove(Direction.DOWN)){
            board.move(Direction.DOWN);
            board.addRandomTile();
            System.out.println("Moving Down");
            removeBoard();
            updateBoard();
          }
        }
        
        //If the key pressed is the left arrow key, check if left is a valid 
        //move, move, add random tile, print indicator in terminal, clear 
        //current board, update gridpane with new board
        if (e.getCode().equals(KeyCode.LEFT)){
          if (board.canMove(Direction.LEFT)){
            board.move(Direction.LEFT);
            board.addRandomTile();
            System.out.println("Moving Left");
            removeBoard();
            updateBoard();
          }
        }
        
        //If the key pressed is the right arrow key, check if right is a valid 
        //move, move, add random tile, print indicator in terminal, clear 
        //current board, update gridpane with new board
        if (e.getCode().equals(KeyCode.RIGHT)){
          if (board.canMove(Direction.RIGHT)){
            board.move(Direction.RIGHT);
            board.addRandomTile();
            System.out.println("Moving Right");
            removeBoard();
            updateBoard();
          }
        }
        
        //If the key pressed is the "q" key, save, then exit the game.
        if (e.getCode().equals(KeyCode.Q)){
          try {
            board.saveBoard(new File("Board1") + ".board");
            System.out.println("Saving Board to Board1.board");
          } 
          catch (IndexOutOfBoundsException i) {
            System.err.println("IndexOutOfBoundsException: " + i.getMessage());
          } 
          catch (IOException i) {
            System.err.println("Caught IOException: " + i.getMessage());
          }
          System.exit(0);
        }

        //If key pressed is  the "s" key, save the game
        if (e.getCode().equals(KeyCode.S)){
          try {
            board.saveBoard("Board1" + ".board");
            System.out.println("Saving Board to Board1.board");
          } 
          catch (IndexOutOfBoundsException i) {
            System.err.println("IndexOutOfBoundsException: " + i.getMessage());
          } 
          catch (IOException i) {
            System.err.println("Caught IOException: " + i.getMessage());
          }
        }

        //print out current state of board
        System.out.println(board.toString());

        //If game over, print new transparent rectangle over gridpane with 
        //game over message
        if (board.isGameOver()){
          
          //If game is over for the first time, then create new transparent
          //rectangle, create new text game over message, add both to gridpane,
          //format both, align both
          if (GOindex == 0){
            System.out.println("Game Over!");
            Rectangle r = new Rectangle(oneThousand,oneThousand);
            r.setFill(COLOR_GAME_OVER);
            gpane.add(r,0,0,four,four);
            GridPane.setHalignment(r, HPos.CENTER);
            GridPane.setValignment(r, VPos.CENTER);
            Text gameOver = new Text("Game Over!");
            gameOver.setFont(Font.font("Helvetica Neue", FontWeight.BOLD, 
              font_c));
            gameOver.setFill(COLOR_VALUE_DARK);
            GridPane.setHalignment(gameOver, HPos.CENTER);
            GridPane.setValignment(gameOver, VPos.CENTER);
            gpane.add(gameOver,0,0,four,four);
            gpane.setAlignment(Pos.CENTER);
            
            //Increment GOindex
            GOindex++; 
          }

          //If game has been over more than once, then return and do nothing 
          else{
            return;
          }
        }
      }
  }

  ///////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////
  /////////////////////** DO NOT EDIT BELOW */////////////////////
  ///////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  
  // The method used to process the command line arguments
  private void processArgs(String[] args){

    String inputBoard = null;   // The filename for where to load the Board
    int boardSize = 0;          // The Size of the Board

    // Arguments must come in pairs
    if((args.length % 2) != 0)
    {
      printUsage();
      System.exit(-1);
    }

    // Process all the arguments 
    for(int i = 0; i < args.length; i += 2){
      if(args[i].equals("-i"))
      {   // We are processing the argument that specifies
        // the input file to be used to set the board
        inputBoard = args[i + 1];
      }
      else if(args[i].equals("-o"))
      {   // We are processing the argument that specifies
        // the output file to be used to save the board
        outputBoard = args[i + 1];
      }
      else if(args[i].equals("-s"))
      {   // We are processing the argument that specifies
        // the size of the Board
        boardSize = Integer.parseInt(args[i + 1]);
      }
      else
      {   // Incorrect Argument 
        printUsage();
        System.exit(-1);
      }
    }

    // Set the default output file if none specified
    if(outputBoard == null)
      outputBoard = "2048.board";
    // Set the default Board size if none specified or less than 2
    if(boardSize < 2)
      boardSize = 4;

    // Initialize the Game Board
    try{
      if(inputBoard != null)
        board = new Board(inputBoard, new Random());
      else
        board = new Board(boardSize, new Random());
    }
    catch (Exception e)
    {
      System.out.println(e.getClass().getName() + " was thrown while creating a " +
          "Board from file " + inputBoard);
      System.out.println("Either your Board(String, Random) " +
          "Constructor is broken or the file isn't " +
          "formated correctly");
      System.exit(-1);
    }
  }

  // Print the Usage Message 
  private static void printUsage()
  {
    System.out.println("Gui2048");
    System.out.println("Usage:  Gui2048 [-i|o file ...]");
    System.out.println();
    System.out.println("  Command line arguments come in pairs of the form: <command> <argument>");
    System.out.println();
    System.out.println("  -i [file]  -> Specifies a 2048 board that should be loaded");
    System.out.println();
    System.out.println("  -o [file]  -> Specifies a file that should be used to save the 2048 board");
    System.out.println("                If none specified then the default \"2048.board\" file will be used");
    System.out.println("  -s [size]  -> Specifies the size of the 2048 board if an input file hasn't been");
    System.out.println("                specified.  If both -s and -i are used, then the size of the board");
    System.out.println("                will be determined by the input file. The default size is 4.");
  }
}
